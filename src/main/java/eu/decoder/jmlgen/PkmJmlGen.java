package eu.decoder.jmlgen;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.openapitools.client.ApiException;
import org.openapitools.client.api.InvocationsApi;
import org.openapitools.client.model.PkmFile;
import org.openapitools.client.model.PkmInvocation;
import org.openapitools.client.model.PkmInvocation.InvocationStatusEnum;
import org.openapitools.client.model.PkmInvocationInvocationResults;

import com.google.gson.JsonObject;

import eu.decoder.jmlgen.generator.JmlGenerator;
import eu.decoder.pkm.client.PkmClient;
import eu.decoder.pkm.client.PkmLogItems;

public class PkmJmlGen {

	private Properties config;

	public PkmJmlGen(Properties config) {
		this.config = config;
	}
	
	public Properties getConfig() {
		return this.config;
	}
	
	/**
	 * Connect to PKM on specified project
	 * @param project the PKM project name (null if specified in configuration)
	 * @param createDbIfNotExists If true, create project if not exists
	 * @return The connected PKM client
	 * @throws ApiException
	 */
	public PkmClient login(String project, boolean createDbIfNotExists) throws ApiException {
		if(project == null) project = getProperty("project");
		if(project == null) project = getProperty("database"); // backward compatibility with previous versions
		if(project == null) {
			// error
			throw new RuntimeException("Error: missing project property");
		}
		
		//String codePath = getProperty("codePath");
		//if(codePath == null) codePath = tmpDir.toString();

		String baseUrl = getProperty("baseUrl");
		if(baseUrl == null) baseUrl = "http://pkm-api_pkm_1:8080";
		
		// Initialize PKM client
		PkmClient client;
		try {
			client = new PkmClient(baseUrl, project, getProperty("certificate"));
		} catch (FileNotFoundException e) {
			throw new ApiException(e);
		}
		
		if(getProperty("apikey") != null) {
			client.login(getProperty("apikey"), createDbIfNotExists);
		} else {
			client.login(getProperty("user"), getProperty("password"), createDbIfNotExists);
		}
		return client;
	}

	/**
	 * Inject JML into the PKM, according to configuration
	 * @param project the PKM project name (null if specified in configuration)
	 * @param invocationId the process engine invocation ID (null if no PE involved)
	 * @param logout true to log out after injection, false otherwise
	 * @throws Exception
	 */
	public void injectJmlIntoPKM(String project, String invocationId, boolean logout, boolean verbose) throws Exception {

		PkmInvocation invocation = null;
		// If the call came from the Process Engine, prepare context to push outcome
		if(invocationId != null && invocationId.trim().length() > 0) {
			invocation = new PkmInvocation();
			invocation.setTool("jmlgen");
			invocation.setInvocationID(invocationId);
			String time = getTime();
			invocation.setTimestampRequest(time);
			invocation.setTimestampStart(time);
		}

		PkmClient client = login(project, true);
		Path tmpDir = Files.createTempDirectory("pkm").toAbsolutePath();
		PkmLogItems pkmLog = new PkmLogItems("jmlgen", "Log");
		
		if(verbose) System.out.println("JmlGen invoked with ID: " + invocationId);
		pkmLog.addLog("JmlGen invoked with ID: " + invocationId);

        // Extract data from PKM, and copy them to temporary dir
		List<PkmFile> pkmFiles = client.getAllSourceFiles(true, ".java");
		HashSet<String> inferredCodePath = new HashSet<String>();
		Set<String> modifiedFiles = null;
		if(pkmFiles != null) {
			// Copy files to temp dir
			for(PkmFile pkmFile : pkmFiles) {
				Path parent = Paths.get(pkmFile.getRelPath()).getParent();
				if(parent == null) {
					String message = "Ignoring file " + pkmFile.getRelPath() + ": parent path unknown";
					System.err.println(message);
					pkmLog.addError(message); // Log error in PKM Logs
				} else {
					String relFileDir = parent.toString().trim();
					// If relative path is absolute, simply make it relative by removing first /
					// By convention, we should have a path relative to the project root
					// (so "/dir/file" has the same meaning as "dir/file")
					while(relFileDir.startsWith(File.separator) && relFileDir.length() > 0) {
						relFileDir = relFileDir.substring(1);
					}
					if(relFileDir.length() > 0) {
						Files.createDirectories(Paths.get(tmpDir.toString(), relFileDir));
						String fileContent = pkmFile.getContent();

						// codePath can be inferred from class package
						inferredCodePath.addAll(inferCodePath(relFileDir, fileContent));

						if(fileContent == null) { // OpenApi version mismatch may cause read issues...
							throw new ApiException("JML injection failed due to null PKM file content: OpenApi version mismatch?");
						}
			
						Files.writeString(Paths.get(tmpDir.toString(), pkmFile.getRelPath()),
								fileContent);
					}
				}
			}
			
			StringBuilder codePath = new StringBuilder();
			boolean first = true;
			for(String path : inferredCodePath) {
				codePath.append((first ? "" : ":") + path);
				first = false;
			}

			if(codePath.length() < 1) codePath.append("src/main/java");

			PrintStream out = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				// Prepare JML generation logging
				out = new PrintStream(baos, true);

				// Generate JML
				JmlGenerator generator = new JmlGenerator();
				modifiedFiles = generator.generateJml(tmpDir.toString(), tmpDir.toString(),
						codePath.toString(),
						out);
			} catch(IOException e) { // FAILURE - log, then throw exception
				pkmLog.addError(e.getMessage());
				// If the call came from the Process Engine, inform it of failure
				if(invocation != null) {
					invocation.setUser(client.getUser());
					invocation.setInvocationConfiguration(new JsonObject());
					invocation.setTimestampCompleted(getTime());
					invocation.setInvocationStatus(InvocationStatusEnum.FAILED);		
					InvocationsApi api = client.getInvocationsApi();
					api.putInvocations(project, client.getApiKey(), Arrays.asList(invocation));
				}
				throw e;
			} finally {
				if(out != null) {
					// Log all JML generation traces into PKM
					String jmlLogs = new String(baos.toByteArray());
					out.close();
					if(verbose) System.out.println(jmlLogs);
					pkmLog.addLog(jmlLogs); // Single log with all stuff...
					//addPkmLogs(pkmLog, jmlLogs); // multiline log
				}
			}
			
			// Put JML-annotated data back to the PKM
			if(verbose) System.out.print("Injecting JML into PKM...");
			pkmLog.addLog("Start injecting JML into PKM");
			for(PkmFile pkmFile : pkmFiles) {
				 // Push only files with JML added
				if(modifiedFiles.contains(pkmFile.getRelPath()) || modifiedFiles.contains(File.separator + pkmFile.getRelPath())) {
					client.postSourceFile(tmpDir.toFile(),
						Paths.get(tmpDir.toString(), pkmFile.getRelPath()).toFile(), true);
				}
			}
			if(verbose) System.out.println("...done!");
			pkmLog.addLog("Done injecting JML into PKM");

			if(! "false".equalsIgnoreCase(getProperty("deleteTempFiles"))) {
				// Delete temp directory recursively
				// See https://www.baeldung.com/java-delete-directory
				Files.walk(tmpDir)
			      .sorted(Comparator.reverseOrder())
			      .map(Path::toFile)
			      .forEach(File::delete);
			}
			if(verbose) System.out.println("Temporary files cleared (to keep them, set deleteTempFiles to false)");
		}
		
		if("true".equalsIgnoreCase(getProperty("mockTest"))) {
			if(verbose) System.out.println("Deleting DB (mockTest set to true in configuration)");
			client.setProjectTransient();
		}

		client.logItems(pkmLog); // Flush PKM log items into database

		// If the call came from the Process Engine, inform it of successful outcome
		if(invocation != null) {
			invocation.setTimestampCompleted(getTime());
			invocation.setUser(client.getUser());
			invocation.setInvocationConfiguration(new JsonObject());
			invocation.setInvocationStatus(InvocationStatusEnum.COMPLETED);

			if(modifiedFiles != null) {
				List<PkmInvocationInvocationResults> modifications = new LinkedList<PkmInvocationInvocationResults>();
				for(String path : modifiedFiles) {
					PkmInvocationInvocationResults m = new PkmInvocationInvocationResults();
					m.setPath(path);
					m.setType("sourcecode");
					modifications.add(m);
				}
				invocation.setInvocationResults(modifications);
			}
	
			if(verbose) System.out.println("JmlGen tell PKM successful outcome for invocation=" + invocationId);
			InvocationsApi api = client.getInvocationsApi();
			api.putInvocations(project, client.getApiKey(), Arrays.asList(invocation));
		}

		if(logout) client.logout();
	}

	/**
	 * Infer code path from relative file dir and java package name in file
	 * @param relPath Path relative to tmpDir
	 * @param content Java file content
	 */
	private HashSet<String> inferCodePath(String relPath, String content) {
		HashSet<String> codePath = new HashSet<>();
		BufferedReader in = null;
		try {
			in = new BufferedReader(new StringReader(content));
			String line;
			boolean found = false;
			while((line = in.readLine()) != null && ! found) {
				line = line.trim();
				if(line.startsWith("package") && line.endsWith(";")) {
					String path = line.substring(8, line.length()-1).trim().replace(".", File.separator);
					if(relPath.equals(path)) {
						codePath.add(".");
					} else if(relPath.endsWith(path)) {
						String pathToAdd = relPath.substring(0, relPath.indexOf(path));
						if(! pathToAdd.endsWith(".")) { //Filter for directory names with "." inside...
							codePath.add(pathToAdd);
						}
					}
				}
			}
		} catch(Exception ignore) {
		} finally {
			if(in != null) try { in.close(); } catch(Exception ignore) { }
		}

		return codePath;
	}
		
	private String getProperty(String property) {
		String result = this.config.getProperty(property);
		return (result == null ? null : result.trim());
	}

	/**
	 * Retrieves current time (expected format: yyyyMMdd_HHmmss)
	 * @return Current time
	 */
	private String getTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}

	/**
	 * Command-line JmlGen injection into PKM
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
        
		if(args.length <= 0) {
			// error
			System.err.println("Usage: PkmJmlGen <propertiesFile>");
			System.exit(1);
		}
		
		Properties config = new Properties();
		config.load(new FileInputStream(args[0]));
		
		PkmJmlGen jmlGenerator = new PkmJmlGen(config);
		jmlGenerator.injectJmlIntoPKM(null, null, true, true);
	
		System.exit(0);
	}

	/**
	 * Log each line of a multiline string into the PKM
	 * @param pkmLogs
	 * @param logs
	 */
	private void addPkmLogs(PkmLogItems pkmLogs, String logs) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new StringReader(logs));
			String log;
			while((log = in.readLine()) != null) {
				pkmLogs.addLog(log);
			}
			in.close();
		} catch(IOException e) {
			if(in != null) try { in.close(); } catch(Exception ignore) { }
		}
	}

}
