# Inject JML annotations in PKM java raw source code

## What is done ?

JML annotations are generated, from what can be guessed out of a java project.
The java code is extracted from the Decoder PKM (RawSourceCode collection), then overwritten in the PKM with JML inside.

See https://gitlab.ow2.org/decoder/jmlgen for JML functionalities.

## Build

```
mvn clean install
```

### Note concerning SNAPSHOT dependencies

Dependencies related to Jml generation and PKM integration are managed on the Gitlab package registry.
The dependency management mechanism should be transparent (like for atitacts deployed on maven central).

If not, the alternative is: clone each project, and build it (mvn clean install).
Dependencies required:
- jmlgen: https://gitlab.ow2.org/decoder/jmlgen
- PKM simple java client: https://gitlab.ow2.org/decoder/pkm-simple-java-client

## Run an example

```
mvn exec:java -Dexec.mainClass="eu.decoder.jmlgen.PkmJmlGen" -Dexec.args="src/main/resources/jmlgen-pkm.properties"
```

Note: Raw java source code must be loaded in the PKM before running JmlGen.

One can upload a full project using PKM simple java client: see https://gitlab.ow2.org/decoder/pkm-simple-java-client#upload-a-project-in-the-pkm .

Concerning the project configuration, the properties file provided should provide PKM location and credentials, for instance as follows - these settings correspond to the dockerized PKM provided in https://gitlab.ow2.org/decoder/pkm-api (can be launched using "docker-compose up" - see documentation there):

```
# Properties file example, to inject JML into PKM using JmlGen

# PKM location, project name and credentials
baseUrl: http://pkm-api_pkm_1:8080
#baseUrl: https://pkm-api_pkm_1:8080
#certificate: src/main/resources/pkm_docker.crt
# Sample based on MyThaiStar, available on https://github.com/devonfw/my-thai-star
# To check, query RawSourceCode collection in DB OW2-MyThaiStar as follows:
# db.getCollection('RawSourcecode').find({'filename':'java/mtsj/api/src/main/java/com/devonfw/application/mtsj/general/logic/api/to/BinaryObjectEto.java'})
project: OW2-MyThaiStar
user: admin
password: admin

#deleteTempFiles: true

# Mock test: if true, project is deleted at logout (default false)
#mockTest: true
```
